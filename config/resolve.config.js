const fs = require('fs');
const _ = require('lodash');
const yaml = require('js-yaml');
const RE2 = require('re2');

const resolveConfig = (filePath) => {
  try {
    // read yml file
    const fileContents = fs.readFileSync(filePath, 'utf8');
    // load yml file to object
    let data = yaml.safeLoad(fileContents);
    // parse object to string for regex search
    const ymltext = JSON.stringify(data);
    // define pattern regex using RE2 packages
    const pattern = new RE2(/\${(\w+)}/g);
    // find array of value that matches to the regex pattern
    const match = pattern.match(ymltext);
    // iterate for replace value in yml object
    for (let i = 0; i < match.length; i++) {
      // extract key value
      const key = pattern.exec(ymltext);
      // find key match and replace its value using environtment variables
      findKey(data, match[i], process.env[key[1]]);
    }
    // log stringify object
    console.log(JSON.stringify(data));
    // return object
    return data;
  } catch (e) {
    console.log(e);
  }
};

function findKey(obj, target, replaceValue) {
  let result = null;
  if (_.isEmpty(obj) || !_.isObject(obj)) {
    return null;
  }
  if (!_.isArray(obj) && Object.keys(obj).length > 0) {
    for (let i = 0; i < Object.keys(obj).length; i++) {
      let key = Object.keys(obj)[i];
      let val = obj[key];
      if (val === target) {
        return (obj[key] = replaceValue);
      } else {
        result = findKey(val, target, replaceValue);
      }
      if (result) {
        break;
      }
    }
  }
  return result;
}

module.exports = {
  resolveConfig,
};
