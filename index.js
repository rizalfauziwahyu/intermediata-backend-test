const path = require('path');
const fs = require('fs');

const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const compression = require('compression');
const morgan = require('morgan');

const apiresponse = require('./middleware/apiresponse');
const { resolveConfig } = require('./config/resolve.config');

const app = express();

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, 'access.log'),
  { flags: 'a' }
);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '4MB' }));
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, csrf-token, Content-Disposition'
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PUT, PATCH, DELETE'
  );

  next();
});
app.use(helmet());
app.use(compression());
app.use(morgan('combined', { stream: accessLogStream }));

const rajaongkirRoutes = require('./routes/api/rajaongkir.route');

app.use('/api/rajaongkir', rajaongkirRoutes);

app.use(apiresponse.errorHandler);

const port =
  process.env.NODE_ENV === 'production' ? process.env.PORT || 80 : 3000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

resolveConfig(path.join(__dirname, 'config', 'service.yml'));