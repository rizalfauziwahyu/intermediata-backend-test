const express = require('express');
const router = express.Router();
const rajaongkirController = require('../../controller/rajaongkir.controller');

router.get('/cities', rajaongkirController.getCities);

router.get('/provinces', rajaongkirController.getProvinces);

module.exports = router;
