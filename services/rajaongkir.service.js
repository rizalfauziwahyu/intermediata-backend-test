const axios = require('axios');

const provinces = async (query) => {
  const config = {
    method: 'GET',
    url: `https://api.rajaongkir.com/starter/province`,
    params: {
      id: query.id,
    },
    headers: {
      key: process.env.RAJA_ONGKIR_KEY,
    },
  };

  const response = await axios(config);

  return response.data.rajaongkir;
};

const cities = async (query) => {
  const config = {
    method: 'GET',
    url: `https://api.rajaongkir.com/starter/city`,
    params: {
      id: query.id,
      province: query.province,
    },
    headers: {
      key: process.env.RAJA_ONGKIR_KEY,
    },
  };

  const response = await axios(config);
  return response.data.rajaongkir;
};

module.exports = {
  provinces,
  cities,
};
