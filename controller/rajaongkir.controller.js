const rajaongkirService = require('../services/rajaongkir.service');
const apiResponse = require('../middleware/apiresponse');

const getProvinces = (req, res, next) => {
  rajaongkirService
    .provinces(req.query)
    .then((data) => {
      const response = apiResponse.getResponse(
        true,
        'get provinces success',
        data
      );
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) err.statusCode = 500;
      next(err);
    });
};

const getCities = (req, res, next) => {
  rajaongkirService
    .cities(req.query)
    .then((data) => {
      const response = apiResponse.getResponse(
        true,
        'get cities success',
        data
      );
      return res.status(200).json(response);
    })
    .catch((err) => {
      if (!err.statusCode) err.statusCode = 500;
      next(err);
    });
};

module.exports = {
  getProvinces,
  getCities,
};
