# Intermediate backend Test.<br>

### Framework
#### [Express](https://docs.nestjs.com/)
Fast, unopinionated, minimalist web framework for Node.js

### API Documentation
#### [POSTMAN Documentation](https://documenter.getpostman.com/view/6320297/TVCcY9UL)

## Get Started

### Precondition
* Node.js (> = 8.9.0)
* Postman

#### <i>Clone</i> Project App

git

```bash
https://gitlab.com/rizalfauziwahyu/intermediata-backend-test.git
```

#### <i> Download dependencies </i>

use [npm](https://www.npmjs.com/) that have installed on your pc.

```bash
npm install
```


#### Running App

Running backend in development mode port 3000.
```bash
npm run start:dev
```

#### Test

##### 1. Algorithmic Test
###### Environtment Variables
![Screen Shot](documentation/env.png)
###### Logic
![Screen Shot](documentation/logic.png)
###### Output
![Screen Shot](documentation/output.png)

##### 2. Backend Engineering Test

###### I have already publish my collection and provide example responses.
###### [POSTMAN Documentation](https://documenter.getpostman.com/view/6320297/TVCcY9UL)

##### 3. Knowledge and Experiences
###### 1. Apa tantangan terbesar yang pernah Anda temui saat membuat web application dari sisi engineering dan bagaimana menyelesaikan permasalahan tersebut? 
Jawab : Tantangan terbesar dalam sisi backend engineering yang pernah saya alami yaitu ketika membuat realtime chat web application menggunakan socket.io, menggunakan socket dan menginisialisasi setiap event listener yang ada serta mengelolanya dengan baik merupakan hal yang cukup menantang bagi saya.
###### 2. Apakah Anda sudah mengetahui tentang clean code? Bagaimana implementasi clean code pada project Anda?.
Jawab : Saya telah mempelajari tentang clean code dimana seorang programmer menulis source code dengan baik sehingga software dapat maintainable dan mudah dikembangkan untuk jangka panjang. Penerapan dalam project laravel saya mengikuti standar / guide line berikut [PHP Guideline](https://github.com/pt-dot/php-guidelines)
###### 3. Apakah Anda menggunakan Git workflow dalam pengerjaan project? Jika ya, jelaskan bagaimana Git workflow yang Anda terapkan.
Jawab : Ya, saya menggunakan Git workflow jika dalam collaborative team, Penerapannya menggunakan branch staging untuk development test, untuk setiap fitur yang akan dibuat maka dibuatkan branch baru dari staging dan ketika fitur selesai maka branch fitur akan di merge oleh branch staging.
###### 4. Apa yang anda ketahui dengan design pattern? Jika pernah menggunakan, jelaskan design pattern apa saja yang biasanya digunakan untuk menyelesaikan masalah software engineering di web application.
Jawab : Design pattern adalah metode penyelesaian masalah yang memiliki pola, saya seringkali menggunakan repository pattern dalam proses membuat web application, memisahkan logic layer dengan controller sangat efisien untuk reusable code, sehingga mempercepat waktu pengerjaan proyek.
###### 5. Apa anda bersedia ditempatkan onsite di Malang? Jika memang harus remote, bagaimana pengaturan waktu & availability dalam komunikasi dan kolaborasi pengerjaan project?.
Jawab : Harus remote, pengaturan waktu dan availability berdasarkan task priority, waktu online available dari jam 7 pagi - jam 9 malam, komunikasi dan kolaborasi bisa dilakukan dengan online conference tools seperti google meet, zoom dan tools yang lainnya. Menyelesaikan tugas harian dari supervisor merupakan prioritas utama saya.


## Contributors
* Muhammad Rizal Fauzi Wahyu
